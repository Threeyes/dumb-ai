﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
[ExecuteInEditMode]
public class LazyBrain : Brain
{
    public override string name
    {
        get
        {
            throw new System.NotImplementedException();
        }
    }

    public override bool Activate
    {
        get
        {
            throw new System.NotImplementedException();
        }
    }

    public override void Do()
    {
        throw new System.NotImplementedException();
    }


    public override void Say(string str)
    {
        Debug.Log(str);
    }


    public void OnClick()
    {
        Say("Stop poking at me!");
    }

    public string info;
    [ContextMenu("Search")]
    void Search()
    {
        Search(string.Format("https://www.baidu.com/#ie=UTF-8&wd={0}", info));
    }
    public void Search(string info)
    {
        Say("Give me a break!");
        Application.OpenURL(info);
    }

    private void Start()
    {
        
    }

    public override void Appearance()
    {
        throw new System.NotImplementedException();
    }
}
