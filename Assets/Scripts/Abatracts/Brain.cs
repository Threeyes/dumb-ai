﻿using UnityEngine;
public abstract class Brain : MonoBehaviour
{
    public abstract bool Activate { get; }
    public new abstract string name { get; }


    public abstract void Appearance();
    public abstract void Do();
    public abstract void Say(string stu);
}
