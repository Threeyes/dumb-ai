﻿public interface IBehaviour  
{
    /// <summary>
    /// Silly things
    /// </summary>
    void Do();

    /// <summary>
    /// nonsense
    /// </summary>
    void Say(string str);
}
