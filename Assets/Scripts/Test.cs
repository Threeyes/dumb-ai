﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Test : MonoBehaviour 
{
    public SceneView sceneView;
    private void Start()
    {
        sceneView = EditorWindow.GetWindow<SceneView>();
    }
    private void Update()
    {
        Rect position = sceneView.position;
        position.width = Mathf.Sin(Time.time) * 300;
        position.height = Mathf.Sin(Time.time) * 500;
        sceneView.position = position;

        if (Input.GetKeyDown(KeyCode.S))
            EditorWindow.FocusWindowIfItsOpen(typeof(SceneView));

        Debug.Log("Hi");
    }

}
